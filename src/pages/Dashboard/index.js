import React, { lazy, Suspense } from 'react';
import Layout from 'components/layout';
import { Switch, Route } from 'react-router-dom';

const Accounts = lazy(() => import('./Accounts'));
const Home = lazy(() => import('./Home'));
const Settings = lazy(() => import('./Settings'));

const Loader = () => <p className='loading'>loading...</p>;

export default props => {
  return (
    <Layout>
      <Route
        exact
        path='/dashboard'
        render={pageProps => (
          <Suspense fallback={Loader}>
            <Home {...pageProps} />
          </Suspense>
        )}
      />
      <Switch>
        <Route
          path='/dashboard/accounts'
          render={pageProps => (
            <Suspense fallback={Loader}>
              <Accounts {...pageProps} />
            </Suspense>
          )}
        />

        <Route
          path='/dashboard/settings'
          render={pageProps => (
            <Suspense fallback={Loader}>
              <Settings {...pageProps} />
            </Suspense>
          )}
        />
      </Switch>
    </Layout>
  );
};
