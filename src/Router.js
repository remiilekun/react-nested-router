import React, { lazy, Suspense, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

const Home = lazy(() => import('pages/Home'));
const Dashboard = lazy(() => import('pages/Dashboard'));

const Scroll = props => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [props.location]);

  return props.children;
};

Scroll.propTypes = {
  location: PropTypes.objectOf(PropTypes.any),
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
};

const ScrollToTop = withRouter(Scroll);

const Loader = () => <div>loading</div>;

const RouterComponent = () => {
  return (
    <Router>
      <ScrollToTop>
        <Switch>
          <Route
            exact
            path='/'
            render={props => (
              <Suspense fallback={<Loader />}>
                <Home {...props} />
              </Suspense>
            )}
          />

          <Route
            path='/dashboard'
            render={props => (
              <Suspense fallback={<Loader />}>
                <Dashboard {...props} />
              </Suspense>
            )}
          />
        </Switch>
      </ScrollToTop>
    </Router>
  );
};

export default RouterComponent;
