import React from 'react';
import { Link } from 'react-router-dom';

const Wrapper = props => <div style={{ minHeight: '100vh', width: '100%', display: 'flex' }}>{props.children}</div>;

const Sidebar = props => (
  <div style={{ height: '100vh', background: '#d2d2d2', padding: '2rem 4rem' }}>
    <ul style={{ margin: 0, padding: 0, listStyle: 'none' }}>
      <li>
        <Link to='/dashboard'>Home</Link>
      </li>

      <li>
        <Link to='/dashboard/accounts'>Accounts</Link>
      </li>

      <li>
        <Link to='/dashboard/Settings'>Settings</Link>
      </li>
    </ul>
  </div>
);

const Content = props => <div style={{ flex: 1, minHeight: '100vh', padding: '10px' }}>{props.children}</div>;

const Layout = ({ children }) => {
  return (
    <Wrapper>
      <Sidebar />
      <Content>{children}</Content>
    </Wrapper>
  );
};

export default Layout;
